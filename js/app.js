btnMode = document.querySelector(".button-style")
btnMode.addEventListener('click', () => {
    btnMode.dataset.mode === "light" ? btnMode.dataset.mode = "dark"
        : btnMode.dataset.mode = "light";
    localStorage.setItem('mode', btnMode.dataset.mode);
    changeTheme(btnMode.dataset.mode);
})

function changeTheme(modeName) {
    const modeUrl = `./css/${modeName}-style.css`
    document.querySelector('[title="mode"]').setAttribute('href', modeUrl);
}

let activeMode = localStorage.getItem('mode');
if (activeMode !== null) {
    changeTheme(activeMode);
    btnMode.dataset.mode = activeMode;
}
